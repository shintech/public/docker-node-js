FROM node:12.13.0

RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app

WORKDIR /home/node/app

COPY package.json .
COPY yarn.lock .

USER node

RUN yarn install --production=true

COPY --chown=node:node . .

CMD yarn start
