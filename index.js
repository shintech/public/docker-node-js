const http = require("http")

const PORT = process.env['PORT'] || 8000

const handleRequest = (req, res) => {
  res.statusCode = 200
  res.setHeader('Content-Type', 'text/plain')
  res.end('Hello World!\n')
}

const onListening = () => {
  console.log(`listening on port: ${PORT}...`)
}

const server = http.createServer(handleRequest)

server.listen(PORT, onListening)
